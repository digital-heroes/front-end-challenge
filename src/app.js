import React from "react";
import { hot } from "react-hot-loader/root";
import Button from "./components/button";
import Heading from "./components/heading";
import Card from "./components/card";

const App = () => (
  <div className="page-wrapper">
    <Heading title="Kinderevents" />
    <Card />
    <Button label="Eventkalender anzeigen" />
  </div>
);

export default hot(App);
