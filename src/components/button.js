import React from "react";
import "./button.scss";
import PropTypes from "prop-types";

const Button = (props) => {
  const { label } = props;

  return (
    <button className="button">
      <span className="button__label" data-hover={label}>
        {label}
      </span>
    </button>
  );
};

export default Button;

Button.propTypes = {
  label: PropTypes.string,
};
