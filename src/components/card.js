import React from "react";
import "./card.scss";
import SvgClock from "../icons/clock";
import SvgArrowDown from "../icons/arrow-down";

const Card = () => {
  return (
    <div className="card">
      <div className="card__image-container">
        <img
          className="card__image"
          src="https://picsum.photos/760/400"
          alt="Ein Text, der das Bild beschreibt."
        />
      </div>
      <div className="card__content">
        <p className="card__date-container">
          <time className="card__date" dateTime="2021-02-02">
            15.02. - 21.02.
          </time>
        </p>
        <div>
          <p className="card__text">
            Zeig, was du drauf hast! Ab auf die Candy-Rollschuhbahn.
          </p>
          <p className="card__time">
            <span className="card__time-icon">
              <SvgClock />
            </span>
            <time dateTime="10:00">10:00 - 16:00 Uhr</time>
          </p>
          <p className="card__details">
            Mit tollem Wettbewerb für grosse und kleine Rollschuh-Profis.
          </p>
        </div>
      </div>
      <div className="card__footer">
        <div className="card__arrow">
          <SvgArrowDown />
        </div>
      </div>
    </div>
  );
};

export default Card;
