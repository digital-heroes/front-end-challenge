import React from "react";
import "./clock.scss";

const SvgClock = (props) => {
  return (
    <svg className="svg-clock" viewBox="0 0 40 40" {...props}>
      <path
        d="M20 0c11.028 0 20 8.973 20 20 0 11.028-8.972 20-20 20S0 31.028 0 20C0 8.973 8.972 0 20 0zm0 2.15C10.158 2.15 2.15 10.159 2.15 20c0 9.842 8.008 17.85 17.85 17.85 9.842 0 17.85-8.008 17.85-17.85 0-9.842-8.008-17.85-17.85-17.85zm0 4.499c.594 0 1.075.481 1.075 1.075v12.722l-6.192 6.191a1.072 1.072 0 01-1.52 0c-.42-.42-.42-1.1 0-1.52l5.562-5.562V7.724c0-.594.48-1.075 1.075-1.075z"
        transform="translate(-156 -460) translate(156 460)"
        fill="#000000"
        stroke="none"
        strokeWidth={1}
        fillRule="evenodd"
      />
    </svg>
  );
};

export default SvgClock;
