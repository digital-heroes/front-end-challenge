import React from "react";
import "./arrow-right.scss";

const SvgArrowRight = (props) => {
  return (
    <svg className="svg-arrow-right" viewBox="0 0 40 28" {...props}>
      <path
        d="M26.506.416a1.39 1.39 0 00-1.983 0 1.4 1.4 0 000 1.964L34.58 12.439H1.39A1.382 1.382 0 000 13.829c0 .773.615 1.409 1.389 1.409H34.58l-10.059 10.04c-.536.555-.536 1.448 0 1.983a1.39 1.39 0 001.984 0l12.44-12.44a1.366 1.366 0 000-1.963L26.506.415z"
        transform="translate(-324 -790) translate(324 784) translate(0 6)"
        fill="#000000"
        fillRule="nonzero"
        stroke="none"
        strokeWidth={1}
      />
    </svg>
  );
};

export default SvgArrowRight;
