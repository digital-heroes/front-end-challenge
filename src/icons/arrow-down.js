import React from "react";
import "./arrow-down.scss";

const SvgArrowDown = (props) => {
  return (
    <svg
      className="svg-arrow-down"
      viewBox="0 0 48 48"
      fillRule="evenodd"
      clipRule="evenodd"
      strokeLinejoin="round"
      strokeMiterlimit={2}
      {...props}>
      <path
        d="M40.368 32.323c.677-.654.677-1.743 0-2.419a1.707 1.707 0 00-2.395 0L25.707 42.17V1.693A1.685 1.685 0 0024.012 0c-.943 0-1.718.75-1.718 1.693V42.17L10.051 29.904c-.677-.654-1.766-.654-2.419 0-.677.676-.677 1.766 0 2.419l15.17 15.169a1.666 1.666 0 002.395 0l15.171-15.169z"
        fillRule="nonzero"
      />
    </svg>
  );
};

export default SvgArrowDown;
