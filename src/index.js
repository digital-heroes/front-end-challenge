import React from "react";
import ReactDOM from "react-dom";
import App from "./app";
import "./index.scss";

const mountNode = document.getElementById("app");
ReactDOM.render(<App />, mountNode);
