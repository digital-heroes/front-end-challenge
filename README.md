# Heroes 
**Front End Challenge**

---

## Building and running on localhost

First install dependencies:

```sh
yarn install
```

To run in hot module reloading mode:

```sh
yarn start
```

To create a production build:

```sh
yarn run build-prod
```

To create a development build:

```sh
yarn run build-dev
```

## Running

Open the file `dist/index.html` in your browser
